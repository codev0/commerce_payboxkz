<?php
/**
 * @file
 * Contains Drupal\commerce_payboxkz\Controller\CommercePayboxkzController.
 */

namespace Drupal\commerce_payboxkz\Controller;

use Drupal\Core\Controller\ControllerBase;

class FirstController extends ControllerBase {
  public function content ()
  {
    return array(
      '#type' => 'markup',
      '#markup' => t('This is my menu linked to custom page'),
    );
  }
}