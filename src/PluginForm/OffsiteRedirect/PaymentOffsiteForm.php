<?php

namespace Drupal\commerce_payboxkz\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;
use Drupal\commerce_payboxkz\Paybox\CommercePayboxkzSignature;

class PaymentOffsiteForm extends BasePaymentOffsiteForm {
  /**
   * {@inheritdoc}
   */
  private function build_redirect_data($order = null) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Pay via PayBox'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $order = $payment->getOrder();

    $redirect_method = $payment_gateway_plugin->getConfiguration()['redirect_method'];

    if ($redirect_method == 'post') {
      $redirect_url = Url::fromRoute('commerce_payboxkz.paybox_redirect_post')->toString();
    }
    else {
      // Gateways that use the GET redirect method usually perform an API call
      // that prepares the remote payment and provides the actual url to
      // redirect to. Any params received from that API call that need to be
      // persisted until later payment creation can be saved in $order->data.
      // Example: $order->setData('my_gateway', ['test' => '123']), followed
      // by an $order->save().
      $order = $payment->getOrder();
      // Simulate an API call failing and throwing an exception, for test purposes.
      // See PaymentCheckoutTest::testFailedCheckoutWithOffsiteRedirectGet().
      if ($order->getBillingProfile()->get('address')->family_name == 'FAIL') {
        throw new PaymentGatewayException('Could not get the redirect URL.');
      }
      $redirect_url = Url::fromRoute('commerce_payboxkz.dummy_redirect_302', [], ['absolute' => TRUE])->toString();
    }
    $data = [];

    $currency_code = $payment->getAmount()->getCurrencyCode();
    if($currency_code == 'RUR')
      $currency_code = 'RUB';

    $amount = $payment->getAmount()->getNumber();
    $amount = (float)$amount;
    $amount = round($amount, 2);

    $strLanguage = $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if($strLanguage != 'ru')
      $strLanguage = 'en';

    $nLifeTime = 86400;
//    if(!empty($payment_gateway_plugin->getConfiguration()['lifetime']))
//      $nLifeTime = $payment_gateway_plugin->getConfiguration()['lifetime'];

    $strSiteUrl = 'http://'.$_SERVER['HTTP_HOST'];
    $strSuccessUrl = $strSiteUrl . '/payboxkz/success';
    $strFailureUrl = $strSiteUrl . '/payboxkz/fail';
    $strResultCheckUrl = $strSiteUrl . '/payboxkz/result';

    $arrFields = array(
      'pg_merchant_id' => '501792', // $payment_gateway_plugin->getConfiguration()["merchant_id"]
      'pg_order_id' => $payment->getOrderId(),
      'pg_currency' => $currency_code,
      'pg_amount' => $amount,
      'pg_lifetime' => $nLifeTime,
      'pg_testing_mode' => $payment_gateway_plugin->getConfiguration()["mode"] == 'test' ? 1 : 0,
      'pg_description' => $_SERVER['HTTP_HOST']." # ".$payment->getOrderId(),
      'pg_user_ip' => $_SERVER['REMOTE_ADDR'],
      'pg_language' => $strLanguage,
      'pg_check_url' => $strResultCheckUrl,
      'pg_result_url' => $strResultCheckUrl,
      'pg_success_url' => $strSuccessUrl,
      'pg_failure_url' => $strFailureUrl,
      'pg_request_method' => 'POST',
      'pg_salt' => rand(21,43433), // Параметры безопасности сообщения. Необходима генерация pg_salt и подписи сообщения.
    );

//    if(!empty($order->mail)){
//      $arrFields['pg_user_email'] = $order->mail;
//      $arrFields['pg_user_contact_email'] = $order->mail;
//    }
    $arrFields['op'] = 'Pay via PayBox';
    $signature = CommercePayboxkzSignature::make('payment.php', $arrFields, '6w8QWLsX3n69uaBt');

    $arrFields['pg_sig'] = $signature;

// $payment_gateway_plugin->getConfiguration()["secret_key"]
//    foreach($arrFields as $paramName => $paramValue){
//      if($paramName != 'op')
//        $data[$paramName] = array('#type' => 'hidden', '#value' => $paramValue);
//    }
    return $this->buildRedirectForm($form, $form_state, 'https://paybox.kz/payment.php', $arrFields, $redirect_method);
  }

  /**
   * Builds the redirect form.
   *
   * @param array $form
   *   The plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $redirect_url
   *   The redirect url.
   * @param array $data
   *   Data that should be sent along.
   * @param string $redirect_method
   *   The redirect method (REDIRECT_GET or REDIRECT_POST constant).
   *
   * @return array
   *   The redirect form, if $redirect_method is REDIRECT_POST.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   *   The redirect exception, if $redirect_method is REDIRECT_GET.
   */
  protected function buildRedirectForm(array $form, FormStateInterface $form_state, $redirect_url, array $data, $redirect_method = self::REDIRECT_GET) {
    if ($redirect_method == self::REDIRECT_POST) {
      $form['#attached']['library'][] = 'commerce_payment/offsite_redirect';
      foreach ($data as $key => $value) {
        $form[$key] = [
          '#type' => 'hidden',
          '#value' => $value,
          // Ensure the correct keys by sending values from the form root.
          '#parents' => [$key],
        ];
      }

      // The key is prefixed with 'commerce_' to prevent conflicts with $data.
      $form['commerce_message'] = [
        '#markup' => '<div class="checkout-help">' . t('Please wait while you are redirected to the payment server. If nothing happens within 10 seconds, please click on the button below.') . '</div>',
        '#weight' => -10,
        // Plugin forms are embedded using #process, so it's too late to attach
        // another #process to $form itself, it must be on a sub-element.
        '#process' => [
          [get_class($this), 'processRedirectForm'],
        ],
        '#action' => $redirect_url,
      ];
    }
    else {
      $redirect_url = Url::fromUri($redirect_url, ['absolute' => TRUE, 'query' => $data])->toString();
      throw new NeedsRedirectException($redirect_url);
    }

    return $form;
  }

  /**
   * Prepares the complete form for a POST redirect.
   *
   * Sets the form #action, adds a class for the JS to target.
   * Workaround for buildConfigurationForm() not receiving $complete_form.
   *
   * @param array $element
   *   The form element whose value is being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed form element.
   */
  public static function processRedirectForm(array $element, FormStateInterface $form_state, array &$complete_form) {
    unset($complete_form["form_id"]);
    unset($complete_form["form_token"]);
    unset($complete_form["form_build_id"]);
    $complete_form['#action'] = $element['#action'];
    $complete_form['#attributes']['class'][] = 'payment-redirect-form';
    unset($element['#action']);
    // The form actions are hidden by default, but needed in this case.
    $complete_form['actions']['#access'] = TRUE;
    foreach (Element::children($complete_form['actions']) as $element_name) {
      $complete_form['actions'][$element_name]['#access'] = TRUE;
    }

    return $element;
  }
}
