<?php

namespace Drupal\commerce_payboxkz\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paybox_offsite_redirect",
 *   label = "Paybox",
 *   display_label = "Paybox",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_payboxkz\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 * )
 */
class OffsiteRedirect extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'redirect_method' => 'post',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // A real gateway would always know which redirect method should be used,
    // it's made configurable here for test purposes.
    $form['redirect_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Redirect method'),
      '#options' => [
        'get' => $this->t('Redirect via GET (302 header)'),
        'post' => $this->t('Redirect via POST'),
      ],
      '#default_value' => $this->configuration['redirect_method'],
    ];

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE
    ];

    $form['lifetime'] = [
      '#type' => 'textfield',
      '#title' => 'Lifetime',
      '#default_value' => 0
    ];

		$form['logo_url']	= [
		  '#type' => 'textfield',
      '#title' => 'Logo',
      '#default_value' => $this->configuration['logo_url']
    ];

		$form['success_message'] = [
		  '#type' => 'textfield',
      '#title' => 'Success message',
      '#default_value' => $this->configuration['success_message']
    ];

		$form['fail_message'] = [
		  '#type' => 'textfield',
      '#title' => 'Fail message',
      '#default_value' => $this->configuration['fail_message']
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => 'Description'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['redirect_method'] = $values['redirect_method'];
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['lifetime'] = $values['lifetime'];
      $this->configuration['logo_url'] = $values['logo_url'];
      $this->configuration['description'] = $values['description'];
      $this->configuration['success_message'] = $values['success_message'];
      $this->configuration['fail_message'] = $values['fail_message'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // @todo Add examples of request validation.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id' => $order->id(),
      'remote_id' => $request->query->get('txn_id'),
      'remote_state' => $request->query->get('payment_status'),
    ]);
    $payment->save();
    drupal_set_message('Payment was processed');
  }

}
